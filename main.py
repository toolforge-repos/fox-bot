import os
import random

import mwclient
import requests


def get_random_wiki():
    wiki_matrix_endpoint = os.environ.get("FOXBOT_WIKI_MATRIX_ENDPOINT", "en.wikipedia.beta.wmflabs.org")
    data = requests.get(
        f"https://{wiki_matrix_endpoint}/w/api.php?action=sitematrix&smtype=language&format=json&formatversion=2"
    ).json()["sitematrix"]

    lang = random.choice(list(data.values()))
    return random.choice(lang["site"])["url"].removeprefix("https://")


def main():
    wiki = get_random_wiki()
    print(f"wiki = {wiki}")
    client = mwclient.Site(
        wiki,
        clients_useragent="fox-bot (tools.fox-bot@toolforge.org)",
        consumer_token=os.environ.get("FOXBOT_CONSUMER_TOKEN"),
        consumer_secret=os.environ.get("FOXBOT_CONSUMER_SECRET"),
        access_token=os.environ.get("FOXBOT_ACCESS_TOKEN"),
        access_secret=os.environ.get("FOXBOT_ACCESS_SECRET"),
    )
    # client.login()

    message = "fox!!!"
    image = "Foxes (4848876670).jpg"
    target = os.environ.get("FOXBOT_TARGET", "TheresNoTime")

    response = client.post(
        action="wikilove",
        title=f"User:{target}",
        type="fox",
        subject="A fox to you!",
        message=message,
        text=f"[[File:{image}|left|150px]]{message} ~~~~<br style=\"clear: both;\"/>",
        tags="",
        token=client.get_token("edit"),
    )

    print(response)


if __name__ == "__main__":
    main()
